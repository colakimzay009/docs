## Release Manager incident guide
Release Managers will encounter incidents as they go through their deployment and release tasks. Involvement will come either at the request of the [EOC or IM](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities), or because Delivery have raised an incident to request help with deployments or releases. 

This guide will explain what to expect and our responsibilities for the different types of incident. 

Be sure to read through the full handbook page to understand the full [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management) process. 

## Useful information/tools to debug the incident

1. Check the diff of the latest deployment to the environment.

   The `/chatops run auto_deploy status` command is useful for this.

   The Gitlab Release Tools Bot puts a comment on the incident issue listing
   the current and previous package on production, along with the diff. This can
   be used for incidents affecting production.

   Also post the diff to the incident issue and incident Slack channel. Someone else might
   see something that you missed.

1. Check if any feature flags have been turned on or turned off.

   The incident issue description has a link called `Feature Flag Changes` which links to logs
   showing feature flag changes in the last few hours.

   Every feature flag change creates an issue in
   https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?scope=all&sort=created_date&state=all,
   so that is also a good place to check. You can filter by labels to check for changes in
   a particular environment only.

## Release Manager support request from EOC or IM 
During ongoing incidents the EOC or IM may request support from release managers using the @release-managers slack handle. 

Release managers should treat this as a top priority request and join the Slack channel as well as the Zoom room if required.

Typically we will be asked to give details about
 1. Any ongoing or recently deployed changes, having a compare link between recent packagaes is helpful
 2. Suitability of the package to rollback. If suitable you may want to recommend this option for fast mitigation of software-change incidents
 
 Remember that every incident issue has a comment on it to provide useful information and links to help answer these questions. 

You may need to take action to prevent further deploys or help to get a mitigation deployed, this would either be a rollback, revert, fix, or hotpatch. Which one will depend on the specific incident. Discuss with the EOC and ask for support from Delivery if you're not sure how to proceed. 

During these incidents please add comments to the incident issue and help the EOC complete the incident summary, timeline, labelling, and identification of corrective actions following the incident. 

If the incident blocked deployments please add the appropriate ["deploys-blocked..." labels.](#tracking-deployment-blockers)

## Release Manager requesting support
Sometimes a Release Manager will need to raise an incident to request help and track blockers to deploys and releases. 

During times when we cannot deploy we're vulnerable because in the event of a high-severity problem we would be unable to quickly deploy a fix. Treat all blockers as high-risk and move fast to unblock. 

All deployment or release blockers should be [considered as S2 incidents](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability)

Follow the instructions to [raise a new incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident)

Once the incident is created:
1. You are the Owner for this incident. Be sure to keep people informed and be pro-active in working towards a resolution
1. Fill in as much information as you have on the incident issue. Assign to yourself
1. Join the incident Slack and consider joining the Zoom bridge
1. Engage with the [EOC](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#when-to-contact-the-current-eoc), [Dev escalation](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#bot-pagerslack-usage), or [Quality On Call](https://about.gitlab.com/handbook/engineering/quality/guidelines/#schedule) as needed to resolve the issue 
1. Keep the incident issue updated with the latest decisions and information uncovered

## Once the incident has been mitigated or resolved
1. Set the incident issue labels to "mitigated" once the issue is solved
1. Add the approapriate labels for rootcause and service
1. Work with engineers to agree on corrective actions following this incident
1. Make sure the summary and timeline sections of the description are fully completed before setting to "Resolved". This will automatically close the incident issue
1. Add tracking for the length of delay we experienced by adding appropriate ["deploys-blocked..." labels.](#tracking-deployment-blockers)

## Tracking deployment blockers
To help track time and patterns in deploymemt blockers any incident or Change Request(CR) that blocks a deployment shoud have appropriate "Deploys-blocked-gstg-X" and "Deploys-blocked-gprd-X" labels added. See [this incident](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5712) for an example. Deployment delays are recorded and analyzed on https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/448#overview 
